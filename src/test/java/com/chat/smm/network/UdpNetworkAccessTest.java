package com.chat.smm.network;

import com.chat.smm.packet.converter.MessageConverter;
import com.chat.smm.packet.message.BaseMessage;
import com.chat.smm.packet.message.Outgoing;
import com.chat.smm.packet.message.TextMessage;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Michal Ziolecki.
 */
public class UdpNetworkAccessTest {
    UdpNetworkAccess udpNetworkAccess = new UdpNetworkAccess(  );
    UdpSender sender = new UdpSender(  );

    @Test
    public void sendAndReceiveTextMessage() throws Exception {
        udpNetworkAccess.start();
        TextMessage textMessageToSend = new TextMessage("1","FROM", "TO","Message");
        udpNetworkAccess.sendMessage( textMessageToSend );
        Thread.sleep( 1000 );
        //tutaj wywowalem geter do pola receiveMsg ale przypisanie nastepuje w receivemessage
        BaseMessage receiveMessage = udpNetworkAccess.getReceiveMsg(); //receivemessage jest null
        Assert.assertEquals( "Send and receive message confrontation", textMessageToSend, receiveMessage );
    }

    @Test
    public void sendAndReceiveOutGoingMessage() throws Exception {
        udpNetworkAccess.start();
        Outgoing textMessageToSend = new Outgoing( "Nick" ); // drugi test na inna wiadomosc
        udpNetworkAccess.sendMessage( textMessageToSend );
        Thread.sleep( 1000 );
        // w udpNetworkAccess wiadomosc jest nieodebrana (null) - sprawdzac z debugerem
        BaseMessage receiveMessage = udpNetworkAccess.getReceiveMsg();
        Assert.assertEquals( "Send and receive message confrontation", textMessageToSend, receiveMessage );
    }
}