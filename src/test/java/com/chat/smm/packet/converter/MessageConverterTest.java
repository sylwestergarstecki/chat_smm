package com.chat.smm.packet.converter;

import com.chat.smm.network.UdpPacket;
import com.chat.smm.packet.message.BaseMessage;
import com.chat.smm.packet.message.MessageType;
import com.chat.smm.packet.message.TextMessage;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class MessageConverterTest {

    @Test
    public void deserializeAndSerialize() throws IOException{
        TextMessage textMessage = new TextMessage("1","FROM", "TO","Message");
        MessageConverter messageConverter = new MessageConverter();
        UdpPacket packet = messageConverter.serialize(textMessage);

        BaseMessage deserializedMessage = messageConverter.deserialize(packet);
        Assert.assertEquals(textMessage,deserializedMessage);

    }

}