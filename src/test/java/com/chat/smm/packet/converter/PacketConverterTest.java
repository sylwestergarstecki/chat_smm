package com.chat.smm.packet.converter;

import com.chat.smm.network.UdpPacket;
import com.chat.smm.packet.message.MessageType;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.time.Clock;

import static org.junit.Assert.*;

/**
 * Created by Michal Ziolecki.
 */
public class PacketConverterTest {
    /**
     * Test to check two method from packetConverter in one time
     * */
    @Test
    public void deserializeAndSerialize() throws Exception {
        // create byte table
        byte [] valueInByte = "123".getBytes();
        // object to send and test
        UdpPacket udpPacketToTest = UdpPacket.builder()
                .type( MessageType.ingoing )
                .length( valueInByte.length )
                .value( valueInByte )
                .build();
        System.out.println(udpPacketToTest.toString());
        // instance to test
        PacketConverter packetConverter = new PacketConverter();
        byte[] tableOfUdpPacketInByte = packetConverter.serialize( udpPacketToTest );
        System.out.println("Length return byte table: " + tableOfUdpPacketInByte.length);
        // test of outgoing and incoming tale of bytes - test for packed
        Assert.assertEquals(3+valueInByte.length, tableOfUdpPacketInByte.length);
        UdpPacket udpPacketIncoming = packetConverter.deserialize( tableOfUdpPacketInByte );
        // test of returned packet
        System.out.println(udpPacketIncoming.toString());
        Assert.assertEquals( udpPacketToTest.getLength(), udpPacketIncoming.getLength() );
        Assert.assertEquals(udpPacketToTest,udpPacketIncoming);
    }
}