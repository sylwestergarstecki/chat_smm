package com.chat.smm.packet.converter;

import com.chat.smm.network.UdpPacket;
import com.chat.smm.packet.message.*;
import org.codehaus.jackson.map.ObjectMapper;
import java.io.IOException;


public class MessageConverter {
    private ObjectMapper objectMapper;

    public MessageConverter() {
        objectMapper = new ObjectMapper();
    }

    /**
     * Method to conver udppacket to Json - Sylwester Garstecki
     **/
    public BaseMessage deserialize(UdpPacket data) throws IOException {

        //String convertedValue = Arrays.toString(data.getValue()); // to correct
        String convertedValue = new String(data.getValue());
        //System.out.println(convertedValue);
        BaseMessage result = null;

        switch (data.getType()) {
            case ingoing:
                result = objectMapper.readValue(convertedValue, Ingoing.class);
                break;
            case outgoing:
                result = objectMapper.readValue(convertedValue, Outgoing.class);
                break;
            case message:
                result = objectMapper.readValue(convertedValue, TextMessage.class);
                break;
            case confirmation:
                result = objectMapper.readValue(convertedValue, MessageConfirmation.class);
                break;
        }


        return result;
    }

    /**
     * Method to convert the message to a text form saved in the JSON format - Sylwester Garstecki
     **/
    public UdpPacket serialize(BaseMessage message) throws IOException {
        UdpPacket udpPacket = new UdpPacket();
        String messageFromJSON = objectMapper.writeValueAsString(message);
        MessageType messageType = null;

        if (message instanceof TextMessage) {
            messageType = MessageType.message;
        }
        if (message instanceof Ingoing) {
            messageType = MessageType.ingoing;
        }
        if (message instanceof Outgoing) {
            messageType = MessageType.outgoing;
        }
        if (message instanceof MessageConfirmation) {
            messageType = MessageType.confirmation;
        }

        udpPacket.setLength(messageFromJSON.length());
        byte[] messageByte = messageFromJSON.toString().getBytes();
        udpPacket.setValue(messageByte);
        udpPacket.setType(messageType);

        return new UdpPacket(udpPacket.getType(), udpPacket.getLength(), udpPacket.getValue());
    }
}
