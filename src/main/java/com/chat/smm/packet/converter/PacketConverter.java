package com.chat.smm.packet.converter;

import com.chat.smm.network.UdpPacket;
import com.chat.smm.packet.message.MessageType;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class PacketConverter {

	/**
	 * Method to deserialize table od bytes to UdpPacket type - Michal Ziolecki
	 * */
	public UdpPacket deserialize(byte[] data) {

		// instatnt of udpPacket - to return
		UdpPacket udpPacket;

		// separate to type of udp packet
		ByteBuffer buffer = ByteBuffer.wrap(data);
		MessageType typeOfMessage = MessageType.getTypeOfMessage(buffer.get(0));
		short length = buffer.getShort(1);
		byte[] dataOfValue = Arrays.copyOfRange(data, 3, length + 3);

		//-- filling the instant of udpPacket by a value
		udpPacket = UdpPacket.builder()
				.type( typeOfMessage )
				.length( length )
				.value( dataOfValue )
				.build();

		return udpPacket;
	}

	/**
	 * Method to serialize instant of UdpPacket into byte table - Michal Ziolecki
	 * */
	public byte[] serialize(UdpPacket message) {

		ByteBuffer buffer = ByteBuffer.allocate(message.getLength() + 3);
		buffer.put(message.getType().getByteOfMessage());
		buffer.putShort((short) message.getLength());
		buffer.put(message.getValue());

		//create and return table of bytes
		return buffer.array();
	}
}
