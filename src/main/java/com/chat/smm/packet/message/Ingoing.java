package com.chat.smm.packet.message;

import com.chat.smm.dto.message.MessageStatus;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode
@Getter
@Setter
@Builder
public class Ingoing extends BaseMessage{
	String nick;
	String status;
	MessageStatus messageStatus;


	public Ingoing(String nick, String status, MessageStatus messageStatus) {
		this.nick = nick;
		this.status = status;
		this.messageStatus = messageStatus;
		super.messageType = MessageType.ingoing;
	}
	public Ingoing() {
		super.messageType = MessageType.ingoing;
	}


}
