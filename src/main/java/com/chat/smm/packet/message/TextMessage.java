package com.chat.smm.packet.message;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode
@Getter
@Setter
public class TextMessage extends BaseMessage {
    String id;
    String from;
    String to;
    String message;

    public TextMessage(String id, String from, String to, String message) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.message = message;
        super.messageType = MessageType.message;
    }

    public TextMessage() {
        super.messageType = MessageType.message;
    }
}
