package com.chat.smm.packet.message;

import com.chat.smm.dto.message.MessageStatus;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode
@Getter
@Setter
public class MessageConfirmation  extends BaseMessage{
    String msgid;
    MessageStatus messageStatus;

    public MessageConfirmation(String msgid, MessageStatus messageStatus) {
        this.msgid = msgid;
        this.messageStatus = messageStatus;
        super.messageType = MessageType.confirmation;
    }

    public MessageConfirmation() {
        super.messageType = MessageType.confirmation;
    }
}
