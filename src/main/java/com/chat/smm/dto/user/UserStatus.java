package com.chat.smm.dto.user;

public enum UserStatus {
	available("available"),
	away("away"),
	notDisturb("notDisturb");

	/***
	 * Constructor and method to change enum status into string - Michal Ziolecki
	 */
	private String status;
	UserStatus(String status){
		this.status = status;
	}

	@Override
	public String toString(){
		return status;
	}



}
