package com.chat.smm.dto.user;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class User {
	private String nick;
	private String statusText;
	private UserStatus status;
}
