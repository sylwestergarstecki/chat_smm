package com.chat.smm.dto.message;

public enum MessageStatus {
	received,
	read
}
