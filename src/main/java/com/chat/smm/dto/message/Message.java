package com.chat.smm.dto.message;

import lombok.*;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.sql.Date;
import java.util.UUID;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Message {
	private UUID identifier;
	private String sender;
	private String recipient;
	private String text;
	private Timestamp date;
}
