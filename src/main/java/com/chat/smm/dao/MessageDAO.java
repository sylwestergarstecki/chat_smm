package com.chat.smm.dao;

import com.chat.smm.dto.message.Message;
import com.chat.smm.dto.user.User;

import java.util.List;

public interface MessageDAO {
	boolean save(Message message);
	List<Message> getAll();
	List<Message> getMessageFromSender(String user);
	List<Message> getMessageFromRecipient (String user);
}
