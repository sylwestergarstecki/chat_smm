package com.chat.smm.dao;

import com.chat.smm.dto.user.User;

import java.util.List;

public interface UserDAO {

	boolean save(User user);

	boolean updateUser(User user);

	List<User> getUsers();

	User getOneUser(String onlyNick);
}
