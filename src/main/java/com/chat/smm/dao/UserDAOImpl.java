package com.chat.smm.dao;


import com.chat.smm.dto.user.User;
import com.chat.smm.dto.user.UserStatus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAOImpl implements UserDAO {

	/**
	 *  Class created by Michal Ziolecki
	 * */
	// names of comulns in user table
	private static final String ID_USER = "id_user";
	private static final String NICK = "nick";
	private static final String STATUSTEXT = "statusText";
	private static final String STATUS = "status";

	private static String CONNECTION_STRING = "jdbc:mysql://localhost:3306/chatdb" +
			"?useUnicode=true&useJDBCCompliantTimezoneShift=true" +
			"&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";

	// insert new user to database
	private static final String insertUserQuery =
			"INSERT INTO users ( nick, statusText, status) VALUES (?,?,?);";
	// update status about user
	private static final String updateUserQuery = "UPDATE Users SET statusText = ? , status= ? WHERE nick = ? ;";
	// find only one user
	private static final String findUserByNickQuery = "SELECT * FROM users WHERE nick= ? ;";
	// show list all users
	private static final String findAllUsersList = "SELECT * FROM Users;";
	// log data
	private String login;
	private String password;
	private String host;
	private int port;
	private String dbname;

	public UserDAOImpl(String login, String password) {
		this.login = login;
		this.password = password;
	}

	public UserDAOImpl(String login, String password, String host, int port, String dbname) {
		this.login = login;
		this.password = password;
		this.host = host;
		this.port = port;
		this.dbname = dbname;
	    CONNECTION_STRING =
				"jdbc:mysql://"+ host +":"+port+"/"+ dbname +
						"?useUnicode=true&useJDBCCompliantTimezoneShift=true" +
						"&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";

	}



	/**
	 * Insert new user to database - Michal Ziolecki
	 */
	public boolean save(User user) {
		boolean insertBool = false;
		try {
			try (Connection connection = DriverManager.getConnection( CONNECTION_STRING, login, password )) {
				try (PreparedStatement preparedStatement =
							 connection.prepareStatement( insertUserQuery, Statement.RETURN_GENERATED_KEYS )) {
					preparedStatement.setString( 1, user.getNick() );
					preparedStatement.setString( 2, user.getStatusText() );
					preparedStatement.setString( 3, user.getStatus().toString() );
					preparedStatement.executeUpdate();
					try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
						if (resultSet.next()) insertBool = true;
					}
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return insertBool;
	}

	/**
	 * Method tu update user status and statusText - Michal Ziolecki
	 */
	@Override
	public boolean updateUser(User user) {
		boolean updateBool = false;
		try {
			try (Connection connection = DriverManager.getConnection( CONNECTION_STRING, login, password )) {
				try (PreparedStatement preparedStatement =
							 connection.prepareStatement( updateUserQuery, Statement.RETURN_GENERATED_KEYS )) {
					preparedStatement.setString( 1, user.getStatusText() );
					preparedStatement.setString( 2, user.getStatus().toString() );
					preparedStatement.setString( 3, user.getNick() );
					if (preparedStatement.executeUpdate() > 0) updateBool = true;
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updateBool;
	}

	/**
	 * Get all users chat from database - Michal Ziolecki
	 */
	public List <User> getUsers() {
		List <User> resultListOfUser = new ArrayList <>();

		try {
			try (Connection connection = DriverManager.getConnection( CONNECTION_STRING, login, password )) {
				try (Statement statement = connection.createStatement()) {
					try (ResultSet resultSet = statement.executeQuery( findAllUsersList )) {
						while (resultSet.next()) {
							User user = User.builder()
									.nick( resultSet.getString( NICK ) )
									.statusText( resultSet.getString( STATUSTEXT ) )
									.status( UserStatus.valueOf( resultSet.getString( STATUS ) )   )
									.build();
							resultListOfUser.add( user );
						}
					}
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultListOfUser;
	}

	/**
	 * Get users details from database by nick - Michal Ziolecki
	 */
	@Override
	public User getOneUser(String onlyNick) {
		User searchUser = null;
		try {
			try (Connection connection = DriverManager.getConnection( CONNECTION_STRING, login, password )) {
				try (PreparedStatement preparedStatement = connection.prepareStatement( findUserByNickQuery )) {
					preparedStatement.setString( 1, onlyNick );
					try (ResultSet resultSet = preparedStatement.executeQuery()) {
					   // if(resultSet.next()){
                            while (resultSet.next()){
                                searchUser = User.builder()
                                        .nick( resultSet.getString( NICK ) )
                                        .statusText( resultSet.getString( STATUSTEXT ) )
                                        .status( UserStatus.valueOf( resultSet.getString( STATUS ) )   )
                                        .build();
                            }
                       // }
					}
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return searchUser;
	}
}
