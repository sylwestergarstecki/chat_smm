package com.chat.smm.dao;

import com.chat.smm.dto.message.Message;
import lombok.Getter;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MessageDAOImpl implements MessageDAO {
    private static final String ID_MESSAGE = "id_message";
    private static final String MESSAGE = "message";
    private static final String DATE_OF_MESSAGE = "date_of_message";
    private static final String IDENTIFIER = "identifier";
    private static final String SENDER = "sender";
    private static final String RECIPIENT = "recipient";
    private static  String CONNECTION_STRING =
            "jdbc:mysql://localhost:3306/chatdb" +
                    "?useUnicode=true&useJDBCCompliantTimezoneShift=true" +
                    "&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
    private static final String insertMessageQuery = "INSERT INTO messages " +
            "(message, date_of_message,identifier, sender, recipient) VALUES (?,time(now()),?,?,?);";
    private static final String findAllMessageQuery = "SELECT * FROM messages;";
    private static final String findMessageQueryBySender = "SELECT * FROM messages WHERE sender = ? order by date_of_message desc;";
    private static final String findMessageQueryByRecipient = "SELECT * FROM messages WHERE recipient = ?  order by date_of_message desc;";
    private String login;
    private String password;
    private String host;
    private int port;
    private String dbname;

    public MessageDAOImpl(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public MessageDAOImpl(String login, String password, String host, int port, String dbname) {
        this.login = login;
        this.password = password;
        this.host = host;
        this.port = port;
        this.dbname = dbname;
        CONNECTION_STRING =
                "jdbc:mysql://"+ host +":"+port+"/"+ dbname +
                        "?useUnicode=true&useJDBCCompliantTimezoneShift=true" +
                        "&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
    }


    /**
     * Method to save message to DB. - Sylwester Garstecki
     */
    public boolean save(Message message) {
        boolean insertBool = false;
        try {
            try (Connection con = DriverManager.getConnection(CONNECTION_STRING, login, password)) {
                try (PreparedStatement statement = con.prepareStatement(insertMessageQuery, Statement.RETURN_GENERATED_KEYS)) {
                    statement.setString(1, message.getText());
                    statement.setString(2, message.getIdentifier().toString());
                    statement.setString(3, message.getSender());
                    statement.setString(4, message.getRecipient());
                    statement.executeUpdate();
                    try (ResultSet resultSet = statement.getGeneratedKeys()) {
                        if (resultSet.next()) {
                            insertBool = true;
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return insertBool;
    }

    /**
     * Method to read all messages from DB. - Sylwester Garstcki
     */
    public List<Message> getAll() {
        List<Message> result = new ArrayList<>();
        try {
            try (Connection con = DriverManager.getConnection(CONNECTION_STRING, login, password)) {
                try (Statement statement = con.createStatement()) {
                    try (ResultSet resultSet = statement.executeQuery(findAllMessageQuery)) {
                        while (resultSet.next()) {
                            Message message = Message.builder()
                                    .text(resultSet.getString(MESSAGE))
                                    .sender(resultSet.getString(SENDER))
                                    .recipient(resultSet.getString(RECIPIENT))
                                    .identifier(UUID.fromString(resultSet.getString(IDENTIFIER)))
                                    .date(resultSet.getTimestamp(DATE_OF_MESSAGE))
                                    .build();
                            result.add(message);
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to read all messages from a given sender. - Sylwester Garstecki
     */
    @Override
    public List<Message> getMessageFromSender(String user) {

            Message message;
            List<Message> result = new ArrayList<>();
            try {
                try (Connection con = DriverManager.getConnection(CONNECTION_STRING, login, password)) {
                    try (PreparedStatement statement = con.prepareStatement(findMessageQueryBySender, Statement.RETURN_GENERATED_KEYS)) {
                        statement.setString(1, user );
                        try (ResultSet resultSet = statement.executeQuery()) {
                            while(resultSet.next()){
                                message = Message.builder()
                                        .text(resultSet.getString(MESSAGE))
                                        .sender(resultSet.getString(SENDER))
                                        .recipient(resultSet.getString(RECIPIENT))
                                        .identifier(UUID.fromString(resultSet.getString(IDENTIFIER)))
                                        .date(resultSet.getTimestamp(DATE_OF_MESSAGE))
                                        .build();
                                result.add(message);
                            }
                        }
                    }

                }
            }catch (SQLException e){
                e.printStackTrace();
            }

        return result;
    }


    /**
     * Method to read all messages from a given recipient. - Sylwester Garstecki
     */
    @Override
    public List<Message> getMessageFromRecipient(String user) {
        Message message;
        List<Message> result = new ArrayList<>();
        try {
            try (Connection con = DriverManager.getConnection(CONNECTION_STRING, login, password)) {
                try (PreparedStatement statement = con.prepareStatement(findMessageQueryByRecipient, Statement.RETURN_GENERATED_KEYS)) {
                    statement.setString(1, user );
                    try (ResultSet resultSet = statement.executeQuery()) {
                        while(resultSet.next()){
                            message = Message.builder()
                                    .text(resultSet.getString(MESSAGE))
                                    .sender(resultSet.getString(SENDER))
                                    .recipient(resultSet.getString(RECIPIENT))
                                    .identifier(UUID.fromString(resultSet.getString(IDENTIFIER)))
                                    .date(resultSet.getTimestamp(DATE_OF_MESSAGE))
                                    .build();
                            result.add(message);
                        }
                    }
                }

            }
        }catch (SQLException e){
            e.printStackTrace();
        }

        return result;
    }
    }
