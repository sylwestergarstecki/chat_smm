package com.chat.smm.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Michal Ziolecki.
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class LoginController implements Initializable{

    @FXML
    private Button loginButton;
    @FXML
    private TextField textField;
    @Getter
    private String login;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void logToChat(ActionEvent actionEvent) {
        if (textField.getText().length() > 0 ) {
            login = textField.getText();
            //System.out.println( "Login test: " + login );
            textField.clear();
            ChatApplicationView.stageLogin.close();
        }
    }

    public void pushEnter(KeyEvent pressedKey) {

        if(pressedKey.getCode() == KeyCode.ENTER && textField.getText().length() > 0){
            login = textField.getText();
            //System.out.println( "Login test: " + login );
            textField.clear();
            ChatApplicationView.stageLogin.close();
        }


    }
}
