package com.chat.smm.gui;

import com.chat.smm.network.UdpNetworkAccess;
import com.chat.smm.packet.message.TextMessage;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import lombok.Setter;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.UUID;

@Setter
public class ChatController implements Initializable {
    UdpNetworkAccess udpNetworkAccess;
    public ChatController() {
         this.udpNetworkAccess = new UdpNetworkAccess();
    }

    @FXML
    private ListView contactList;
    @FXML
    private ListView messageList;
    @FXML
    Button buttonSend;
    @FXML
    private TextField textField;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void sendMessageButton(ActionEvent actionEvent) {

        // protection to send empty message
        if(textField.getText().length() > 0){
            String messageToSend = textField.getText();
            UUID messageToSendId = UUID.randomUUID();
            TextMessage textMessage = new TextMessage(messageToSendId.toString(),
                    udpNetworkAccess.getLoginController().getLogin(), "To", messageToSend);
            try{
                udpNetworkAccess.sendMessage(textMessage);
            }catch (IOException e){
                e.printStackTrace();
            }
        }

    }

    public ObservableList<String> items = FXCollections.observableArrayList("Chat - SM");

    public void addMessageToMessageList(TextMessage textMessage){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                String message = textMessage.getMessage();
                String sender = textMessage.getFrom();
                // String receipient = textMessage.getTo();
                //messageList.getItems().add(message);
                if (!items.equals(messageList.getItems())) {
                    messageList.setItems(items);
                }
                items.add(sender + ":   " + message);
            }
        });

    }

    public void sendByEnter(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER && textField.getText().length() > 0){
            String messageToSend = textField.getText();
            UUID messageToSendId = UUID.randomUUID();
            TextMessage textMessage = new TextMessage(messageToSendId.toString(),
                    udpNetworkAccess.getLoginController().getLogin(), "To", messageToSend);
            try{
                udpNetworkAccess.sendMessage(textMessage);
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
