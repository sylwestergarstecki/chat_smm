package com.chat.smm.gui;

import com.chat.smm.network.UdpNetworkAccess;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import javafx.scene.Scene;

public class ChatApplicationView extends Application {

    Scene loginScene;
    static Stage stageLogin;


    @Override
    public void start(Stage stage) throws Exception {
        //create login window
        FXMLLoader fxmlLoginWindow = new FXMLLoader( getClass().getResource( "/fxml/Login.fxml" ) );
        Parent loginWindow = fxmlLoginWindow.load();
        loginScene = new Scene( loginWindow );
        stageLogin = new Stage(  );
        stageLogin.setScene( loginScene );
        stageLogin.setTitle( "Logowanie do Chat'a SM" );
        stageLogin.showAndWait();
        LoginController loginController = fxmlLoginWindow.<LoginController>getController();

        //create chat window
        FXMLLoader fxmlLoader =new FXMLLoader(getClass().getResource("/fxml/chatWindow.fxml"));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root);
        stage.setTitle("Chat SM");
        stage.setScene(scene);
        stage.show();

        ChatController chatController = fxmlLoader.<ChatController>getController();
        UdpNetworkAccess udpNetworkAccess = new UdpNetworkAccess();
        udpNetworkAccess.setChatController( chatController );
        udpNetworkAccess.setLoginController( loginController );
        chatController.setUdpNetworkAccess(udpNetworkAccess);
        udpNetworkAccess.start();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
