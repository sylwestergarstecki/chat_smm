package com.chat.smm.network;

import com.chat.smm.packet.converter.MessageConverter;
import com.chat.smm.packet.converter.PacketConverter;
import com.chat.smm.packet.message.BaseMessage;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.function.Consumer;

public class UdpReceiver extends Thread {
	Consumer<BaseMessage> consumer;
	private MessageConverter messageConverter = null;
	private PacketConverter packetConverter = null;
	private MulticastSocket socket = null;
	private InetAddress address = null;
	private byte[] receiveByteArray = null;
	String ipAddress = null;
	Integer portNumber = null;
	UdpPacket udpPacket= null;

	/**
	 * Basic constructor with default ip address and port /Michal Z
	 * */
	public UdpReceiver(Consumer<BaseMessage> consumer) {
		super();
		this.consumer = consumer;
		this.packetConverter = new PacketConverter();
		this.messageConverter = new MessageConverter();
		this.ipAddress = "239.255.255.255";
		this.portNumber = 9999;
	}
	/**
	 * Constructor to set ip address and port /Michal Z
	 * */
	public UdpReceiver(Consumer<BaseMessage> consumer, String ipAddress, int portNumber) {
		super();
		this.consumer = consumer;
		this.packetConverter = new PacketConverter();
		this.messageConverter = new MessageConverter();
		this.ipAddress = ipAddress;
		this.portNumber = portNumber;
	}

	@Override
	public void run ()
	{
		try{
			this.socket = new MulticastSocket( portNumber );
			this.address = InetAddress.getByName( ipAddress );
			socket.joinGroup( address );
			this.receiveByteArray = new byte[256];
			while(true){

				try {
					DatagramPacket datagramPacket = new DatagramPacket( receiveByteArray,receiveByteArray.length );
					socket.receive( datagramPacket );
					udpPacket = packetConverter.deserialize( datagramPacket.getData() );
					// I dont know why, but need to cast basemessage into basemessage
					//BUG PROBABLY !!!! BELOW - PROBLEM WITH CASTING
					BaseMessage message =  messageConverter.deserialize( udpPacket );
					consumer.accept( message );
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}catch (IOException e){
			e.getStackTrace();
		}
	}

	/**
	 * setter and getter /Michal Z
	 * */
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Integer getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(Integer portNumber) {
		this.portNumber = portNumber;
	}
}
