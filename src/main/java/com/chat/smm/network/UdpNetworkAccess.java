package com.chat.smm.network;

import com.chat.smm.dto.message.Message;
import com.chat.smm.dto.message.MessageStatus;
import com.chat.smm.dto.user.User;
import com.chat.smm.gui.ChatController;
import com.chat.smm.gui.LoginController;
import com.chat.smm.packet.converter.MessageConverter;
import com.chat.smm.packet.converter.PacketConverter;
import com.chat.smm.packet.message.*;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;

import static com.chat.smm.dto.message.MessageStatus.received;


/**
 * Klasa jest fasadą (wzorzec strukturalny) umożliwiającą wysyłanie i odbieranie wiadomości UDP.
 * W razie konieczności dodaj potrzebne metody
 */
public class UdpNetworkAccess {
    // Klasa zapewniająca funkcjonalność wysyłąnia wiadomości UDP
    private UdpSender sender;
    // Klasa zapeniająca funkcjonalność odbierania wiadomości UDP
    private UdpReceiver receiver;
    // pole do wiadomosci
    private BaseMessage receiveMsg;
    private MessageConverter messageConverter;
    @Setter
    ChatController chatController;
    @Setter
    @Getter
    LoginController loginController;


    /**
     *  Constructor with default address and port
     */
    public UdpNetworkAccess() {
        receiver = new UdpReceiver((msg) -> receiveMessage((BaseMessage) msg));
        this.sender = new UdpSender(  );
        this.messageConverter = new MessageConverter();
    }

    /**
     * ToDo Zaimplementuj i zmodyfikuj konstruktor - Lukasz
     * Konstruktor powinien zostać zmodyfikowany tak aby umożliwić przekazanie adresu muticast oraz portu,
     * na który mają być wysyłane wiadomości.
     */
    /**
     * Answer:
     * Constructor with parameters to set address and port - Michal Z
     */
    public UdpNetworkAccess(String ipAddress, int port) {
        receiver = new UdpReceiver((msg) -> receiveMessage((BaseMessage) msg), ipAddress, port);
        this.sender = new UdpSender( ipAddress,port );
    }


    /**
     * Metoda wysyłająca wiadomość tekstową UDP.
     * @param message
     * @return bool (true if send was correct)
     */
    public boolean sendMessage(BaseMessage message) throws IOException {
        // Convert dto.message do UdpMessage();
        UdpPacket packet = new MessageConverter().serialize(message);
        return sender.sendMessage(packet);
}
    /**
     * Włącza odbiorcę (nasłuch) wiadomości UDP. Po uruchomieniu odbiorcy UdpReceiver otrzymuje pakiet i wywołuje
     * metodę receiveMessage. Odbiorca będzie nasłuchiwał na adresie oraz porcie przekazanym w konstrukturze klasy
     * {@link UdpNetworkAccess}
     */
    public void start() {
        receiver.start();

    }

    /**
     * ToDo Zaimplementuj metodę odbierającą pakiet
     * Tutaj dodaj kod który ma się wykonać po odebraniu pakietu
     * Po odebraniu pakietu należy sprawdzić jaki to pakiet i odpowiednio go zinterpretować.
     * Przykładowo jeśli pakiet to {@link TextMessage} to należy go wyświetlić w oknie GUI lub w konsoli
     * z datą odebrania wiadomości, użytkownikiem który wiadomość wysłał oraz treścią wiadomości.
     */
    public void receiveMessage(BaseMessage baseMessage) {

        // checktype of packet (MessageType) and print to console
        // - wyswietlam wszytkie pola dla wiadomosci danego typu
        if (baseMessage instanceof TextMessage) {
            receiveMsg =  baseMessage;
            TextMessage receiveTextMessage = (TextMessage) receiveMsg;
            chatController.addMessageToMessageList( receiveTextMessage );
            // drukowanie do konsoli
            System.out.println(receiveTextMessage.getId() + receiveTextMessage.getFrom() +
                    receiveTextMessage.getTo() + receiveTextMessage.getMessage());
        }
        else if (baseMessage instanceof Ingoing) {
            receiveMsg =  baseMessage;
            Ingoing receiveInGoingMessage = (Ingoing) receiveMsg;
            // drukowanie do konsoli
            System.out.println(receiveInGoingMessage.getNick()+ receiveInGoingMessage.getStatus()
                    + receiveInGoingMessage.getMessageStatus());
        }
        else if (baseMessage instanceof Outgoing) {
            receiveMsg =  baseMessage;
            Outgoing receiveOutGoingMessage = (Outgoing) receiveMsg;
            // drukowanie do konsoli
            System.out.println(receiveOutGoingMessage.getNick());
        }
        else if (baseMessage instanceof MessageConfirmation) {
            receiveMsg =  baseMessage;
            MessageConfirmation receiveConfirmationMessage = (MessageConfirmation) receiveMsg;
            // drukowanie do konsoli
            System.out.println(receiveConfirmationMessage.getMsgid() + receiveConfirmationMessage.getMessageStatus());
        }
    }

    public boolean confirmMessage(MessageConfirmation confirmation) throws IOException {
        /**
         * ToDo Zaimplementuj metodę wysyłającą potwierdznie odebrania pakietu
         * Tutaj dodaj kod, który ma się wykonać aby wysłać wiadomość potwierdzającą odebranie wiadomości
         */

        UdpPacket udpPacketConfirmation = messageConverter.serialize(confirmation);
        return sender.sendMessage(udpPacketConfirmation);
    }

    public boolean leaveChat(Outgoing message) throws IOException {
        /**
         * ToDo Zaimplementuj metodę wysyłającą informację o wyłączeniu czata
         * Tutaj dodaj kod, który ma się wykonać aby wysłać wiadomość informującą wszystkich użytkowników
         * o wyłączeniu aplikacji czat.
         */
        UdpPacket udpPacketLeave = messageConverter.serialize(message);
        return sender.sendMessage(udpPacketLeave);
    }

    public boolean joinChat(Ingoing message) throws IOException {
        /**
         * ToDo Zaimplementuj metodę wysyłającą informację o włączeniu czata
         * Tutaj dodaj kod, który ma się wykonać aby wysłać wiadomość informującą wszystkich użytkowników
         * o włączeniu aplikacji czat. Ten pakiet informuje pozostałych użytkowników, że nowy użytkownik pojawił się
         */
        UdpPacket udpPacketJoin = messageConverter.serialize(message);
        return sender.sendMessage(udpPacketJoin);
    }

    public BaseMessage getReceiveMsg() {
        return receiveMsg;
    }

    public void setReceiveMsg(BaseMessage receiveMsg) {
        this.receiveMsg = receiveMsg;
    }
}
