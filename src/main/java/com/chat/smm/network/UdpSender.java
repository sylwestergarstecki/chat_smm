package com.chat.smm.network;

import com.chat.smm.packet.converter.PacketConverter;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpSender {

	private static DatagramSocket socket = null;
	String ipAddress = null;
	Integer portNumber = null;

	/**
	 * Basic constructor with default ip address and port /Michal Z
	 * */
	public UdpSender(){
		this.ipAddress = "239.255.255.255";
		this.portNumber = 9999;
	}

	/**
	 * Constructor to set ip address and port /Michal Z
	 * */
	public UdpSender(String ipAddress, int portNumber){
		this.ipAddress = ipAddress;
		this.portNumber = portNumber;
	}

	/**
	 * Method to send message - Sylwester Garstecki /Michal Z
	 * */

	public boolean sendMessage(UdpPacket packet){
		//convert UdpPacket to byte[]
		PacketConverter packetConverter = new PacketConverter();
		byte[] messageByteToSend = packetConverter.serialize(packet);
		boolean test = sendMessage( messageByteToSend );

		return test;

	}

	private boolean sendMessage(byte[] data){

		boolean test = false;

		try{
			socket = new DatagramSocket();
			socket.setBroadcast(true);

			DatagramPacket message =
					new DatagramPacket( data, data.length, InetAddress.getByName(ipAddress), portNumber);

			socket.send(message);
			socket.close();
			test = true;

		}catch(IOException e){
			e.getStackTrace();
		}

		return test;
	}

	/**
	 * setter and getter /Michal Z
	 * */

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Integer getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(Integer portNumber) {
		this.portNumber = portNumber;
	}
}
